


class Sensor {
  int id;
  String title;
  num temperature;
  num humidity;
  String createdAt;
  String updatedAt;
  String deletedAt;


  Sensor({this.id,this.title,this.temperature,this.humidity,this.createdAt,this.updatedAt,this.deletedAt});

  factory Sensor.fromJson(Map<String, dynamic> json) {
    return Sensor(
      id: json['id'],
      title: json['title'],
      temperature: json['temperature'],
      humidity: json['humidity'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      deletedAt: json['deleted_at'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'temperature': temperature,
      'humidity': humidity,
      'created_at': createdAt,
      'updated_at': updatedAt,
      'deleted_at': deletedAt,
    };
  }

}