import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';



class Apis {
  static const String sensors = '/sensors';
}
@RestApi(baseUrl: "https://arduino.openumeric.com/api/")
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  @GET(Apis.sensors)
  Future<ResponseData> getSensors();
}

//Run the command: flutter pub run build_runner build pour avoir _ApiClient & ResponseData